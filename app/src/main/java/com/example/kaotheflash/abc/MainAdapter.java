package com.example.kaotheflash.abc;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by KaOtheflash on 2/2/2561.
 */

public class MainAdapter extends BaseAdapter{
    private Context context;
    private int[] intsPics;
    private  String[] stringsName;

    public MainAdapter(Context context, int[] intsPics, String[] stringsName) {
        this.context = context;
        this.intsPics = intsPics;
        this.stringsName = stringsName;
    }

    @Override
    public int getCount() {
        return intsPics.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view1 = layoutInflater.inflate(R.layout.listview_layout,viewGroup,false);
        ImageView imageView =  view1.findViewById(R.id.imageView);
        TextView Name = view1.findViewById(R.id.textView2);

        imageView.setImageResource(intsPics[i]);
        Name.setText(stringsName[i]);
        return view1;
    }
}
