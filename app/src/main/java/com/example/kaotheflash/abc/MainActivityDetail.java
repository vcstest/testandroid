package com.example.kaotheflash.abc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivityDetail extends AppCompatActivity {

    private Button Btnbc2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_detail);

        Btnbc2 = findViewById(R.id.Btn5);

        Btnbc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivityDetail.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
