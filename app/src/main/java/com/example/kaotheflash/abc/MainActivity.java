package com.example.kaotheflash.abc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button Btnaz;
    private  Button Btncre;
    private  Button Btnbc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Btnaz = findViewById(R.id.Btn3);
        Btncre = findViewById(R.id.Btn2);
        Btnbc = findViewById(R.id.Btn1);

        Btnaz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MainActivityaz.class);
                startActivity(intent);
                finish();
            }
        });

        Btncre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MainActivityDetail.class);
                startActivity(intent);
                finish();
            }
        });

        Btnbc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                System.exit(0);
            }
        });
    }
}
